import React, { Component } from "react";
import "./main.css";
import RocketMan from "../images/agencyGoRocketMan.svg";

class Home extends Component {
  render() {
    return (
      <div className="section-title">
        <div className="left">
          <div className="img">
            <img src={RocketMan} />
          </div>
        </div>
        <div className="right">
          <h1>เราจะทำให้ชีวิตคุณดีขึ้น</h1>
          <p>
            ชีวิตคนเรานั้นแสนสั้น อย่าไปเสียเวลา ให้กับงานง่าย ๆ อย่าง
            การเก็บข้อมูลจาก Influencer หรือการทำ Report แบบเดิม ๆ
            ที่ต้องใช้เวลานาน ๆ
          </p>
        </div>
      </div>
    );
  }
}

export default Home;
