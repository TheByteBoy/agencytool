import React from "react";
import { Route, Switch } from "react-router-dom";
import Nav from "../components/Nav";
import Home from "./Home";
import Features from "./Features";

function App() {
  return (
    <div>
      <Nav />
      <Switch>
        <Route path="/features">
          <Features />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
