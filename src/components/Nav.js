import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Nav.css";
import Logo from "../images/agencygo.svg";

class Nav extends Component {
  render() {
    return (
      <div className="main-nav">
        <div className="left">
          <Link to="/">
            <img src={Logo} className="logo" alt="agencygo" />
          </Link>
        </div>

        <div className="right">
          <Link to="/">Home</Link>
          <Link to="/features">Features</Link>
        </div>
      </div>
    );
  }
}

export default Nav;
